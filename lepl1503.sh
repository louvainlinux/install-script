#!/bin/bash
# test if sudo
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
# This script will install all dependencies for LEPL1503

apt update && sudo apt upgrade -y
apt install -y python3 python3-pip python3-venv python-is-python3 build-essential
apt install -y git
apt-get install gpg
#yes no question for installing VSCode
read -p "Do you want to install VSCode? (yes/no)" yn
case $yn in
    yes ) wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg; install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg; sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'; rm -f packages.microsoft.gpg; ;;
    no ) ;;
esac

#yes no question for installing CLion
read -p "Do you want to install CLion? (yes/no)" yn
case $yn in
    yes )
    echo "[Desktop Entry]
    Name=CLion 2022.3.1
    Icon=/home/$USER/clion-2020.2.3/bin/clion.svg
    StartupWMClass=jetbrains-clion
    Comment=A cross-platform C and C++ IDE
    Exec=\"/home/$USER/clion-2020.2.3/bin/clion.sh\" %u
    Version=1.0
    Type=Application
    Categories=Development;IDE;
    Terminal=false
    StartupNotify=true" > $HOME/.local/share/applications/clion.desktop; ;;
    no ) ;;
esac


snap remove firefox
add-apt-repository ppa:mozillateam/ppa
echo "Package: firefox*
Pin: release o=Ubuntu*
Pin-Priority: -1
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 999" > /etc/apt/preferences.d/firefox-nosnap
apt install firefox
echo "Unattended-Upgrade::Allowed-Origins:: \"LP-PPA-mozillateam:${distro_codename}\";" > /etc/apt/apt.conf.d/50unattended-upgrades-firefox
